const gulp = require('gulp');
const babelify = require('babelify');
const browserify = require('browserify')
const source = require('vinyl-source-stream');
const buffer = require('vinyl-buffer');

var superstatic = require('superstatic');
var browserSync = require('browser-sync').create();
var htmlInjector = require("bs-html-injector");

// const uglify = require('gulp-uglify');
const webserver = require('gulp-webserver');

var gutil = require("gulp-util");
var webpack = require("webpack");
var WebpackDevServer = require("webpack-dev-server");


gulp.task('es6', () => {
	browserify('src/app.js')
		.transform('babelify', {
			presets: ['es2015']
		})
		.bundle()	
		.pipe(source('bundle.js'))
		.pipe(buffer())
		.pipe(gulp.dest('./dist/'));
});

/**
 * Start Browsersync
 */
gulp.task("browser-sync", function () {
	browserSync.use(htmlInjector, {
		files: ["./index.html", "src/**"]
	});
    browserSync.init({
        server: "./"
    });
});


gulp.task('serve', function() {
  browserSync.init({
    server: {
      middleware: [superstatic({stack: 'strict'})]
    }
  });
  gulp.watch(["./index.html", "src/*.js"]).on('change', browserSync.reload);
});


gulp.task('default', ['es6','serve'],() => {
	gulp.watch('src/app.js',['es6'])
});



